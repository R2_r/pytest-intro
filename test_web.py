﻿def test_page_title(driver):
    """Checks if web page has correct title"""
    assert driver.title == "QA Labs - Warsztaty dla Specjalistów IT"
