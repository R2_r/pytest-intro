﻿import pytest
import requests
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager


@pytest.fixture()
def pypl_data():
    """Reads PYPL data from CSV file"""
    with open("pypl.csv") as f:
        content = f.read()
    lines = content.split("\n")
    return lines


@pytest.fixture()
def total_shares(pypl_data):
    """Calculates total per cent of PYPL shares"""
    total = 0
    for line in pypl_data[1:]:
        values = line.split(",")
        total += int(values[3])
    return total


@pytest.fixture()
def api_response():
    """Makes HTTP request to GitLab API"""
    url = "https://gitlab.com/api/v4/projects/11463782/repository/commits"
    payload = {"ref_name": "3f998241", "with_stats": True}
    r = requests.get(url, params=payload)
    return r


@pytest.fixture()
def http_status_code(api_response):
    """Gets HTTP response status code"""
    status_code = api_response.status_code
    return status_code


@pytest.fixture()
def json_content(api_response):
    """Gets HTTP response JSON content"""
    data = api_response.json().pop()
    return data


@pytest.fixture()
def driver():
    """Returns browser driver with loaded web page"""
    url = "https://qalabs.pl"
    d = Firefox(service=Service(GeckoDriverManager().install()))
    d.implicitly_wait(0.5)
    d.get(url)
    return d
